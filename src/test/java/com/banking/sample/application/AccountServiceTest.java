package com.banking.sample.application;

import com.banking.sample.domain.Money;
import com.banking.sample.domain.account.Account;
import com.banking.sample.domain.account.Repository;
import org.junit.jupiter.api.Test;
import org.mockito.ArgumentCaptor;
import org.mockito.Mockito;
import org.mockito.internal.matchers.apachecommons.ReflectionEquals;

import java.util.UUID;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

class AccountServiceTest {

    @Test
    void createAccountNoBalance() {
        Repository repository = Mockito.mock(Repository.class);
        AccountService service = new AccountService(repository);

        ArgumentCaptor<Account> argument = ArgumentCaptor.forClass(Account.class);
        Account account = service.createAccount("test", null);
        Mockito.verify(repository).save(argument.capture());

        assertTrue(new ReflectionEquals(new Account.Builder()
                .setName("test")
                .setStatus(Account.Status.ENABLED)
                .build(), "id", "registered").matches(account));

        assertEquals(account, argument.getValue());
    }

    @Test
    void createAccountBalance() {
        Repository repository = Mockito.mock(Repository.class);
        AccountService service = new AccountService(repository);

        ArgumentCaptor<Account> argument = ArgumentCaptor.forClass(Account.class);
        Account account = service.createAccount("test", new Money(1., "USD"));

        assertEquals(account.getBalance().getValue(), new Money(1., "USD"));

    }

    @Test
    void listAccounts() {
        Repository repository = Mockito.mock(Repository.class);
        AccountService service = new AccountService(repository);

        service.list();
        Mockito.verify(repository).listAll();
    }

    @Test
    void get() {
        Repository repository = Mockito.mock(Repository.class);
        AccountService service = new AccountService(repository);

        UUID id = UUID.randomUUID();
        service.get(id);
        Mockito.verify(repository).findByID(id);
    }
}