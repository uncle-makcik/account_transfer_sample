package com.banking.sample.application;

import com.banking.sample.domain.Money;
import com.banking.sample.domain.NotEnoughMoney;
import com.banking.sample.domain.account.Account;
import com.banking.sample.domain.account.IncompatibleState;
import com.banking.sample.domain.operations.OperationNotAllowed;
import com.banking.sample.domain.operations.Repository;
import com.banking.sample.domain.operations.Transfer;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.mockito.internal.matchers.apachecommons.ReflectionEquals;

import java.util.UUID;

import static org.junit.jupiter.api.Assertions.*;

class TransferServiceTest {

    @Test
    void list() {
        Repository transferRepository = Mockito.mock(Repository.class);
        com.banking.sample.domain.account.Repository accountRepository =
                Mockito.mock(com.banking.sample.domain.account.Repository.class);

        TransferService transferService = new TransferService(transferRepository, accountRepository);

        transferService.list();

        Mockito.verify(transferRepository).listAll();
    }

    @Test
    void get() {
        Repository transferRepository = Mockito.mock(Repository.class);
        com.banking.sample.domain.account.Repository accountRepository =
                Mockito.mock(com.banking.sample.domain.account.Repository.class);

        TransferService transferService = new TransferService(transferRepository, accountRepository);

        UUID id = UUID.randomUUID();
        transferService.get(id);

        Mockito.verify(transferRepository).findByID(id);
    }

    @Test
    void create() {
        Repository transferRepository = Mockito.mock(Repository.class);
        com.banking.sample.domain.account.Repository accountRepository =
                Mockito.mock(com.banking.sample.domain.account.Repository.class);
        Account source = Mockito.mock(Account.class);
        Account destination = Mockito.mock(Account.class);
        Money amount = new Money(1., "USD");
        UUID sourceID = UUID.randomUUID();
        UUID destinationID = UUID.randomUUID();
        Mockito.when(accountRepository.findByID(sourceID)).thenReturn(source);
        Mockito.when(accountRepository.findByID(destinationID)).thenReturn(destination);

        TransferService transferService = new TransferService(transferRepository, accountRepository);

        Transfer transfer = transferService.create(sourceID, destinationID, amount);
        assertTrue(new ReflectionEquals(new Transfer(source, destination, amount), "id", "created")
                .matches(transfer));
    }

    @Test
    void revert() throws NotEnoughMoney, OperationNotAllowed, IncompatibleState {
        Repository transferRepository = Mockito.mock(Repository.class);
        com.banking.sample.domain.account.Repository accountRepository =
                Mockito.mock(com.banking.sample.domain.account.Repository.class);
        Transfer transfer = Mockito.mock(Transfer.class);
        UUID id = UUID.randomUUID();
        Mockito.when(transferRepository.findByID(id)).thenReturn(transfer);

        TransferService transferService = new TransferService(transferRepository, accountRepository);

        transferService.revert(id);

        Mockito.verify(transfer).revert();
        Mockito.verify(transferRepository).save(transfer);
    }

    @Test
    void perform() throws NotEnoughMoney, OperationNotAllowed, IncompatibleState {
        Repository transferRepository = Mockito.mock(Repository.class);
        com.banking.sample.domain.account.Repository accountRepository =
                Mockito.mock(com.banking.sample.domain.account.Repository.class);
        Transfer transfer = Mockito.mock(Transfer.class);
        UUID id = UUID.randomUUID();
        Mockito.when(transferRepository.findByID(id)).thenReturn(transfer);

        TransferService transferService = new TransferService(transferRepository, accountRepository);

        transferService.perform(id);

        Mockito.verify(transfer).perform();
        Mockito.verify(transferRepository).save(transfer);
    }
}