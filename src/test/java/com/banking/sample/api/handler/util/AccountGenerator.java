package com.banking.sample.api.handler.util;

import com.banking.sample.api.model.Account;
import com.banking.sample.api.model.Money;
import com.networknt.exception.ClientException;

import java.io.IOException;
import java.math.BigDecimal;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.UUID;

public class AccountGenerator {
    public static UUID generate(String uri, String name, double balance) throws IOException, ClientException, URISyntaxException {

        return new EntityGenerator(new URI(uri)).generate("/v1/accounts", new Account() {{
            setName(name);
            setBalance(new Money(BigDecimal.valueOf(balance), "USD"));
        }});
    }
}
