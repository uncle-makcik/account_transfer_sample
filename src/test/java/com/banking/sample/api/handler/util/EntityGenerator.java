package com.banking.sample.api.handler.util;

import com.banking.sample.api.model.Account;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.networknt.client.Http2Client;
import com.networknt.config.Config;
import com.networknt.exception.ClientException;
import com.networknt.status.Status;
import io.undertow.client.ClientConnection;
import io.undertow.client.ClientRequest;
import io.undertow.client.ClientResponse;
import io.undertow.util.Headers;
import io.undertow.util.Methods;
import io.undertow.util.StatusCodes;
import org.junit.Assert;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.xnio.IoUtils;
import org.xnio.OptionMap;

import java.io.IOException;
import java.net.URI;
import java.util.UUID;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.atomic.AtomicReference;

public class EntityGenerator {
    @JsonIgnoreProperties(ignoreUnknown = true)
    private static class EntityId {
        @JsonProperty("id")
        UUID id;
    }
    private URI url;
    static final Logger logger = LoggerFactory.getLogger(EntityGenerator.class);
    static final String JSON_MEDIA_TYPE = "application/json";

    public EntityGenerator(URI url) {
        this.url = url;
    }

    public <T> UUID generate(String path, T payload) throws ClientException, IOException {
        final Http2Client client = Http2Client.getInstance();
        final CountDownLatch latch = new CountDownLatch(1);
        final ClientConnection connection;
        try {
            connection = client.connect(url, Http2Client.WORKER, Http2Client.BUFFER_POOL, OptionMap.EMPTY).get();
        } catch (Exception e) {
            throw new ClientException(e);
        }
        final AtomicReference<ClientResponse> reference = new AtomicReference<>();
        final var mapper = Config.getInstance().getMapper();
        try {
            ClientRequest request = new ClientRequest().setPath(path).setMethod(Methods.POST);

            request.getRequestHeaders().put(Headers.HOST, "localhost");
            request.getRequestHeaders().put(Headers.CONTENT_TYPE, JSON_MEDIA_TYPE);
            request.getRequestHeaders().put(Headers.TRANSFER_ENCODING, "chunked");

            connection.sendRequest(request, client.createClientCallback(reference, latch,
                    mapper.writeValueAsString(payload)));

            latch.await();
        } catch (Exception e) {
            logger.error("Exception: ", e);
            throw new ClientException(e);
        } finally {
            IoUtils.safeClose(connection);
        }
        String body = reference.get().getAttachment(Http2Client.RESPONSE_BODY);
        int statusCode = reference.get().getResponseCode();
        Assert.assertEquals(StatusCodes.CREATED, statusCode);

        EntityId response = mapper.readValue(body, EntityId.class);

        return response.id;
    }
}
