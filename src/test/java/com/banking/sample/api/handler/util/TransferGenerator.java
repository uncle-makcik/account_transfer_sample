package com.banking.sample.api.handler.util;

import com.banking.sample.api.model.Money;
import com.banking.sample.api.model.Transfer;
import com.networknt.exception.ClientException;

import java.io.IOException;
import java.math.BigDecimal;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.UUID;

public class TransferGenerator {
    public static UUID generate(String uri, String name, double amount) throws IOException, ClientException, URISyntaxException {

        UUID source = AccountGenerator.generate(uri, name + " source", 2.);
        UUID destination = AccountGenerator.generate(uri, name + " destination", 1.);

        return new EntityGenerator(new URI(uri)).generate("/v1/transfers", new Transfer() {{
            setSource(source.toString());
            setDestination(destination.toString());
            setAmount(new Money(BigDecimal.valueOf(amount), "USD"));
        }});
    }
}
