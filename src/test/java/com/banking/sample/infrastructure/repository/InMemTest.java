package com.banking.sample.infrastructure.repository;

import com.banking.sample.domain.Identifiable;
import org.junit.jupiter.api.Test;

import java.util.List;
import java.util.NoSuchElementException;
import java.util.UUID;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.*;
import static org.hamcrest.core.IsIterableContaining.hasItems;
import static org.junit.jupiter.api.Assertions.assertThrows;

class InMemTest {

    static final List<UUID> ids = List.of(UUID.randomUUID(), UUID.randomUUID());

    final class Entity implements  Identifiable
    {
        private UUID id;

        public Entity(UUID id) {
            this.id = id;
        }

        @Override
        public UUID getId() {
            return id;
        }
    }

    @Test
    void listAll() {
        InMem<Entity> repository = new InMem<>();

        repository.save(new Entity(ids.get(0)));
        repository.save(new Entity(ids.get(1)));

        List<Entity> entities = repository.listAll();
        assertThat(entities, hasItems(hasProperty("id", is(ids.get(1))),
                hasProperty("id", is(ids.get(0)))));
    }

    @Test
    void findByID() {
        InMem<Entity> repository = new InMem<>();

        repository.save(new Entity(ids.get(0)));
        assertThat(repository.findByID(ids.get(0)), hasProperty("id", is(ids.get(0))));
    }

    @Test
    void findByIDNotFound() {
        InMem<Entity> repository = new InMem<>();

        repository.save(new Entity(ids.get(0)));
        assertThrows(NoSuchElementException.class, () -> repository.findByID(ids.get(1)));
    }

    @Test
    void saveUpdate() {
        InMem<Entity> repository = new InMem<>();

        repository.save(new Entity(ids.get(0)));
        repository.save(new Entity(ids.get(0)));
        assertThat(repository.listAll(), hasSize(1));
    }

    @Test
    void lock() {
        InMem<Entity> repository = new InMem<>();

        Entity entity = new Entity(ids.get(0));
        repository.save(entity);

        repository.lock(entity);
    }

    @Test
    void lockUnknown() {
        InMem<Entity> repository = new InMem<>();

        Entity entity = new Entity(ids.get(0));

        assertThrows(NoSuchElementException.class, () -> repository.lock(entity));
    }

    @Test
    void unlockNotLocked() {
        InMem<Entity> repository = new InMem<>();

        Entity entity = new Entity(ids.get(0));
        repository.save(entity);

        repository.unlock(entity);
    }
}