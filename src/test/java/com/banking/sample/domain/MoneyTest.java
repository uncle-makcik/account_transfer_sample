package com.banking.sample.domain;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class MoneyTest {

    @Test
    void addTest() {
        Money left = new Money(1., "USD");
        Money right = new Money(1., "EUR");

        assertEquals(Money.add(left, right), new Money(2., "USD"));
    }

    @Test
    void subtractTest() {
        Money left = new Money(1., "USD");
        Money right = new Money(1., "EUR");

        assertEquals(Money.subtract(left, right), new Money(0., "USD"));
    }

    @Test
    void compareToLess() {
        Money left = new Money(1., "USD");
        Money right = new Money(2., "EUR");

        assertEquals(left.compareTo(right), -1);
    }

    @Test
    void compareToGreater() {
        Money left = new Money(2., "USD");
        Money right = new Money(1., "EUR");

        assertEquals(left.compareTo(right), 1);
    }

    @Test
    void compareToEqual() {
        Money left = new Money(1., "USD");
        Money right = new Money(1., "EUR");

        assertEquals(left.compareTo(right), 0);
    }

    @Test
    void equals() {
        Money left = new Money(1., "USD");
        Money right = new Money(1., "EUR");

        assertEquals(left, right);
    }
}