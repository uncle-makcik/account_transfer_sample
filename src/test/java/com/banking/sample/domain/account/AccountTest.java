package com.banking.sample.domain.account;

import com.banking.sample.domain.Money;
import com.banking.sample.domain.NotEnoughMoney;
import org.mockito.Mockito;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.ArgumentMatchers.any;

class AccountTest {

    @Test
    void hasEnoughMoneyForOperation() {
        Money value = new Money(1., "USD");
        Balance balance = Mockito.mock(Balance.class);
        Mockito.when(balance.isEnoughForOperation(value)).thenReturn(true);

        assertTrue(new Account.Builder()
                .setBalance(balance)
                .build()
                .hasEnoughMoneyForOperation(value));

        Mockito.verify(balance).isEnoughForOperation(value);
    }

    @Test
    void credit() throws IncompatibleState {
        Money amount = new Money(1., "USD");
        Balance balance = Mockito.mock(Balance.class);
        Balance newBalance = Mockito.mock(Balance.class);
        Account account = Mockito.spy(new Account.Builder()
                .setBalance(balance)
                .setStatus(Account.Status.ENABLED)
                .build());
        Mockito.when(balance.credit(amount)).thenReturn(newBalance);
        account.credit(amount);

        Mockito.verify(balance).credit(amount);
        Mockito.verify(account).setBalance(newBalance);
    }

    @Test
    void creditIncompatibleState() {
        Money amount = new Money(1., "USD");
        Balance balance = Mockito.mock(Balance.class);
        Account account = Mockito.spy(new Account.Builder()
                .setBalance(balance)
                .build());
        assertThrows(IncompatibleState.class, () -> account.credit(amount));
    }

    @Test
    void debit() throws NotEnoughMoney, IncompatibleState {
        Money amount = new Money(1., "USD");
        Balance balance = Mockito.mock(Balance.class);
        Balance newBalance = Mockito.mock(Balance.class);
        Account account = Mockito.spy(new Account.Builder()
                .setBalance(balance)
                .setStatus(Account.Status.ENABLED)
                .build());
        Mockito.when(balance.debit(amount)).thenReturn(newBalance);
        account.debit(amount);

        Mockito.verify(balance).debit(amount);
        Mockito.verify(account).setBalance(newBalance);
    }

    @Test
    void debitIncompatibleState() {
        Money amount = new Money(1., "USD");
        Balance balance = Mockito.mock(Balance.class);
        Account account = Mockito.spy(new Account.Builder()
                .setBalance(balance)
                .build());
        assertThrows(IncompatibleState.class, () -> account.debit(amount));
    }

}