package com.banking.sample.domain.account;

import com.banking.sample.domain.Money;
import com.banking.sample.domain.NotEnoughMoney;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;

import static org.junit.jupiter.api.Assertions.*;

class BalanceTest {

    @Test
    void isEnoughForOperationLess() {
        assertFalse(new Balance(new Money(1., "USD"))
                .isEnoughForOperation(new Money(2., "USD")));
    }

    @Test
    void isEnoughForOperationGreater() {
        assertTrue(new Balance(new Money(2., "USD"))
                .isEnoughForOperation(new Money(1., "USD")));
    }

    @Test
    void isEnoughForOperationEquals() {
        assertTrue(new Balance(new Money(2., "USD"))
                .isEnoughForOperation(new Money(1., "USD")));
    }

    @Test
    void checkEnoughForOperationNotEnough() {
        Money amount = new Money(1., "USD");
        Balance balance = Mockito.spy(new Balance(amount));
        Mockito.when(balance.isEnoughForOperation(amount)).thenReturn(false);

        assertThrows(NotEnoughMoney.class, () -> balance.checkEnoughForOperation(amount));
    }

    @Test
    void credit() {
        Money amount = new Money(1., "USD");
        Balance balance = new Balance(amount);

        assertEquals(balance.credit(amount).getValue(), new Money(2., "USD"));
    }

    @Test
    void debit() throws NotEnoughMoney {
        Money amount = new Money(1., "USD");
        Balance balance = Mockito.spy(new Balance(amount));

        assertEquals(balance.debit(amount).getValue(), new Money(0., "USD"));

        Mockito.verify(balance).checkEnoughForOperation(amount);
    }
}