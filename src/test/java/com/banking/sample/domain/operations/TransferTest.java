package com.banking.sample.domain.operations;

import com.banking.sample.domain.Money;
import com.banking.sample.domain.NotEnoughMoney;
import com.banking.sample.domain.account.Account;
import com.banking.sample.domain.account.IncompatibleState;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;

import static org.junit.jupiter.api.Assertions.*;

class TransferTest {

    @Test
    void perform() throws NotEnoughMoney, IncompatibleState, OperationNotAllowed {
        Account source = Mockito.mock(Account.class);
        Account destination = Mockito.mock(Account.class);
        Money amount = new Money(1., "USD");

        Transfer transfer = new Transfer(source, destination, amount);

        transfer.perform();

        Mockito.verify(source).debit(amount);
        Mockito.verify(destination).credit(amount);
        assertTrue(transfer.isPerformed());
    }

    @Test
    void performNotAllowed() throws NotEnoughMoney, IncompatibleState, OperationNotAllowed {
        Account source = Mockito.mock(Account.class);
        Account destination = Mockito.mock(Account.class);
        Money amount = new Money(1., "USD");

        Transfer transfer = new Transfer(source, destination, amount);

        transfer.perform();

        assertThrows(OperationNotAllowed.class, () -> transfer.perform());
    }

    @Test
    void revert() throws OperationNotAllowed, NotEnoughMoney, IncompatibleState {
        Account source = Mockito.mock(Account.class);
        Account destination = Mockito.mock(Account.class);
        Money amount = new Money(1., "USD");

        Transfer transfer = new Transfer(source, destination, amount);

        transfer.perform();
        transfer.revert();

        Mockito.verify(source).credit(amount);
        Mockito.verify(destination).debit(amount);
        assertFalse(transfer.isPerformed());
    }

    @Test
    void revertNotAllowed() throws OperationNotAllowed, NotEnoughMoney, IncompatibleState {
        Account source = Mockito.mock(Account.class);
        Account destination = Mockito.mock(Account.class);
        Money amount = new Money(1., "USD");

        Transfer transfer = new Transfer(source, destination, amount);

        transfer.perform();
        transfer.revert();
        assertThrows(OperationNotAllowed.class, () -> transfer.revert());
    }
}