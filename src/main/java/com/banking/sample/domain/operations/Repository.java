package com.banking.sample.domain.operations;

import java.util.List;
import java.util.NoSuchElementException;
import java.util.UUID;

/**
 * Interface for a repository of transfer operations
 */
public interface Repository {
    /**
     * Get all transfer operation, registered in the system
     * @return List of transfer operations
     */
    List<Transfer> listAll();

    /**
     * Get a transfer operation by specified id
     * @param id - id of an account to get
     * @throws NoSuchElementException - in case there is no transfer with given id
     * @return account
     */
    Transfer findByID(UUID id);

    /**
     * Save a transfer operation
     * Operation unlocks transfer instance
     * @param transfer - transfer operation to be saved
     */
    void save(Transfer transfer);

    /**
     * Take exclusive access to transfer
     * @throws NoSuchElementException in case of account not found
      * @param transfer - instance to lock
     */
    void lock(Transfer transfer);

    /**
     * Release transfer instance
     * @throws NoSuchElementException in case of account not found
     * @param transfer - instance to unlock
     */
    void unlock(Transfer transfer);
}
