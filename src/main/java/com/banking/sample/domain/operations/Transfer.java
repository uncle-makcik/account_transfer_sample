package com.banking.sample.domain.operations;

import com.banking.sample.domain.Identifiable;
import com.banking.sample.domain.Money;
import com.banking.sample.domain.NotEnoughMoney;
import com.banking.sample.domain.account.Account;
import com.banking.sample.domain.account.IncompatibleState;

import java.time.Clock;
import java.time.LocalDateTime;
import java.util.UUID;

/**
 * Transfer money between accounts
 * Transfer is described by source and destination accounts and amount of money to be transferred.
 * Transfer increases destination and decreases source balance.
 * Transfer is performad in two phases
 * <ul>
 * <li>Order</li>
 * <li>Preform</li>
 * </ul>
 */
public class Transfer implements Identifiable {
    private Account source;
    private Account destination;
    private Money amount;
    private boolean performed = false;

    /**
     * Unique ID of a transfer operation. A way of materialization it from DB is out of scope for sake of simplicity
     */
    private UUID id = UUID.randomUUID();

    private LocalDateTime created = LocalDateTime.now(Clock.systemUTC());
    /**
     *
     * @param source - Account to be transferred from
     * @param destination - Account to be transferred to
     * @param amount - money to be transferred
     */
    public Transfer(Account source, Account destination, Money amount) {
        this.source = source;
        this.destination = destination;
        this.amount = amount;
    }

    public Account getSource() {
        return source;
    }

    public Account getDestination() {
        return destination;
    }

    public Money getAmount() {
        return amount;
    }

    public boolean isPerformed() {
        return performed;
    }

    public UUID getId() {
        return id;
    }

    public LocalDateTime getCreated() {
        return created;
    }

    private void checkCanPerform() throws OperationNotAllowed {
        if (isPerformed()) {
            throw new OperationNotAllowed();
        }
    }

    /**
     * Performs a transfer
     * Transfer is implemented as debit for source and credit for destination
     * When operation completed, transfer chenges status to performed.
     * Transaction management is external.
     * @throws OperationNotAllowed attempt to perform transfer more then once
     * @throws NotEnoughMoney - source account balance is not enough
     * @throws IncompatibleState - either source or destination is not enabled
     */
    public void perform() throws NotEnoughMoney, IncompatibleState, OperationNotAllowed {
        checkCanPerform();

        source.debit(amount);
        destination.credit(amount);
        performed = true;
    }

    private void checkCanRevert() throws OperationNotAllowed {
        if (!isPerformed()) {
            throw new OperationNotAllowed();
        }
    }

    /**
     * Reverts already performed transfer
     * @throws OperationNotAllowed attempt to revert not performed transfer
     * @throws NotEnoughMoney - destination account balance is not enough
     * @throws IncompatibleState - either source or destination is not enabled
     */
    public void revert() throws OperationNotAllowed, NotEnoughMoney, IncompatibleState {
        checkCanRevert();

        source.credit(amount);
        destination.debit(amount);
        performed = false;
    }
}
