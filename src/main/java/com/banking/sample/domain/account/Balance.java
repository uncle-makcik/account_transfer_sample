package com.banking.sample.domain.account;

import com.banking.sample.domain.Money;
import com.banking.sample.domain.NotEnoughMoney;

/**
 * Account balance in a specific currency
 *
 * A balance can be credited and debited by some amount.
 * For the sample purposes Balance cannot be debited below zero.
 */
public class Balance {
    private Money value;

    public Balance(Money value) {
       this.value = value;
    }

    public Money getValue() {
        return value;
    }

    /**
     * A validation is the balance is enough for a debit operation.
     * To ensure this current balance amount must be not less then the operation amount.
     * @param amount - debit operation amount
     * @return - true if balance value is enough
     */
    public boolean isEnoughForOperation(Money amount) {
        return this.value.compareTo(amount) >= 0;
    }

    /**
     * A validation is the balance is enough for a debit operation.
     * To ensure this current balance amount must be not less then the operation amount.
     * @throws NotEnoughMoney in case of n enough money on source account balance
     * @param amount - debit operation amount
     */
    public void checkEnoughForOperation(Money amount) throws NotEnoughMoney {
        if (!isEnoughForOperation(amount)) {
            throw new NotEnoughMoney(value, amount);
        }
    }

    /**
     * Credit balance for specified amount.
     * @param amount - amount to credit
     * @return New balance value, increased by amount
     */
    public Balance credit(Money amount) {
        return new Balance(Money.add(value, amount));
    }

    /**
     * Debit balance for specified amount.
     * Before debiting it checks if current value is enough for the operation.
     * @throws NotEnoughMoney in case of balance is not enough to proceed
     * @param amount - amount to debit
     * @return New balance value, decreased by amount
     */
    public Balance debit(Money amount) throws NotEnoughMoney {
        checkEnoughForOperation(amount);

        return new Balance(Money.subtract(value, amount));
    }
}
