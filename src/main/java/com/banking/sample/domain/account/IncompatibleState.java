package com.banking.sample.domain.account;

public class IncompatibleState extends Exception {
    private Account.Status status;
    public IncompatibleState(Account.Status status) {
        this.status = status;
    }
}
