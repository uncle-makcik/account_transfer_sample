package com.banking.sample.domain.account;

import com.banking.sample.domain.Identifiable;
import com.banking.sample.domain.Money;
import com.banking.sample.domain.NotEnoughMoney;

import java.time.Clock;
import java.time.LocalDateTime;
import java.util.UUID;

/**
 * Account. Aggregate root, holds account balance.
 */
public class Account implements Identifiable {
    /**
     * Account status
     */
    public enum Status {
        /**
         * Account is ready for operations
         */
        ENABLED,
        /**
         * Account is disabled by administrator, no operation is permitted.
         */
        DISABLED,
        /**
         * Account is on hold due to negative balance, no operation is permitted.
         */
        CREDIT_HOLD
    }

    private Balance balance;
    private Status status;
    private String name;

    /**
     * Unique ID of an account. A way of materialization it from DB is out of scope for sake of simplicity
     */
    private UUID id = UUID.randomUUID();
    private LocalDateTime registered = LocalDateTime.now(Clock.systemUTC());

    public UUID getId() {
        return id;
    }

    public Account(String name, Balance balance, Status status) {
        this.balance = balance;
        this.status = status;
        this.name = name;
    }

    public Balance getBalance() {
        return balance;
    }

    public void setBalance(Balance balance) {
        this.balance = balance;
    }

    public Status getStatus() {
        return status;
    }

    public String getName() {
        return name;
    }

    public LocalDateTime getRegistered() {
        return registered;
    }

    /**
     * Check if account has enough balance for an operation.
     *
     * @param amount - amount for a proposed operation
     * @return true if money is enough
     */
    public boolean hasEnoughMoneyForOperation(Money amount) {
        return balance.isEnoughForOperation(amount);
    }

    private void checkStatus() throws IncompatibleState {
        if (getStatus() != Status.ENABLED) {
            throw new IncompatibleState(getStatus());
        }
    }

    /**
     * Credit account for a specified amount.
     * Operation checks if account is enabled.
     * @param amount - operation amount
     * @throws IncompatibleState in case of account is not enabled
     */
    public void credit(Money amount) throws IncompatibleState {
        checkStatus();
        setBalance(balance.credit(amount));
    }

    /**
     * Debit account for a specified amount.
     * Operation checks if account is enabled.
     * Operation checks if account has enough balance to finish an operation.
     * @param amount - operation amount
     * @throws NotEnoughMoney in case of account balance is not enough
     * @throws IncompatibleState in case of account is not enabled
     */
    public void debit(Money amount) throws NotEnoughMoney, IncompatibleState {
        checkStatus();
        setBalance(balance.debit(amount));
    }

    /**
     * Builder for Account.
     */
    public static class Builder {
        private Balance balance;
        private Status status;
        private String name;

        public Builder setBalance(Balance balance) {
            this.balance = balance;
            return this;
        }

        public Builder setStatus(Status status) {
            this.status = status;
            return this;
        }

        public Builder setName(String name) {
            this.name = name;
            return this;
        }

        public Account build() {
            return new Account(name, balance, status);
        }
    }
}
