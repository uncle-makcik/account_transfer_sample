package com.banking.sample.domain.account;

import java.util.List;
import java.util.NoSuchElementException;
import java.util.UUID;

/**
 * Interface for a repository of accounts
 */
public interface Repository {
    /**
     * Get all accounts, registered in the system
     * @return List of accounts
     */
    List<Account> listAll();

    /**
     * Get an account by specified id
     * @param id - id of an account to get
     * @throws NoSuchElementException - in case there is no transfer with given id
     * @return account
     */
    Account findByID(UUID id);

    /**
     * Save an account
     * @param account - account to be saved
     */
    void save(Account account);

    /**
     * Take exclusive access to account
     * @throws NoSuchElementException in case of account not found
     * @param account - account to take access
     */
    void lock(Account account);

    /**
     * Release account
     * @throws NoSuchElementException in case of account not found
     * @param account - locked account to release
     */
    void unlock(Account account);
}
