package com.banking.sample.domain;

import java.util.Optional;

/**
 * Money representation.
 *
 * The class is oversimplified for the sample purposes. For the real applications JSR 354 is more suitable.
 * No validation is implemented.
 */
public class Money implements Comparable<Money> {
    private double amount;
    private String currencyCode;

    /**
     * Constructor
     * @param amount - amount of money
     * @param currencyCode - ISO 4217 currency code
     */
    public Money(double amount, String currencyCode) {
        this.currencyCode = currencyCode;
        this.amount = amount;
    }

    public double getAmount() {
        return amount;
    }

    public String getCurrencyCode() {
        return currencyCode;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Money money = (Money) o;

        return Double.compare(money.amount, amount) == 0;
    }

    @Override
    public int hashCode() {
        int result;
        long temp;
        temp = Double.doubleToLongBits(amount);
        result = (int) (temp ^ (temp >>> 32));
        result = 31 * result + (currencyCode != null ? currencyCode.hashCode() : 0);
        return result;
    }

    /**
     * Arithmetical addition of two @link Money instances
     * The operation does not change any of the operands
     *
     * The method does not check if operands are of the same currency
     * @param left - left operand of the operation
     * @param right - left operand of the operation
     * @return - a new Money instance with summarized amount
     */
    public static Money add(Money left, Money right) {

        return new Money(left.amount + right.amount , left.currencyCode);
    }

    /**
     * Arithmetical subtraction of two Money instances
     * The operation does not change any of the operands
     *
     * The method does not check if operands are of the same currency
     * @param left - left operand of the operation
     * @param right - left operand of the operation
     * @return - a new @link Money instance with residue amount
     */
    public static Money subtract(Money left, Money right) {
        return new Money(left.amount - right.amount , left.currencyCode);
    }

    /**
     * Compares amount of Money
     * The method does not check if operands are of the same currency
     * @param o - object to be compared to
     * @return - -1 if this instance is less than o, 0 if it's equal and 1 if it's greater
     */
    public int compareTo(Money o) {
        if (equals(o)) {
            return 0;
        }

        return (amount < o.amount) ? -1 : 1;
    }
}
