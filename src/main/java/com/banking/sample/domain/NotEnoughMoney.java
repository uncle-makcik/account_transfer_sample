package com.banking.sample.domain;

public class NotEnoughMoney extends Exception {
    private Money value;
    private Money amount;

    public NotEnoughMoney(Money value, Money amount) {
        this.value = value;
        this.amount = amount;
    }

    public Money getValue() {
        return value;
    }

    public Money getAmount() {
        return amount;
    }
}
