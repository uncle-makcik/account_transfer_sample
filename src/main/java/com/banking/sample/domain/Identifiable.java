package com.banking.sample.domain;

import java.util.UUID;

public interface Identifiable {
    UUID getId();
}
