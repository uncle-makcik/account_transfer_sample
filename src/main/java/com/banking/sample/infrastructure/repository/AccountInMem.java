package com.banking.sample.infrastructure.repository;

import com.banking.sample.domain.account.Account;
import com.banking.sample.domain.account.Repository;

/**
 * Im-memory {@link Account} repository
 */
public class AccountInMem extends InMem<Account> implements Repository {
}
