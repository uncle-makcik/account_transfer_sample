package com.banking.sample.infrastructure.repository;

import com.banking.sample.domain.Identifiable;

import java.util.List;
import java.util.NoSuchElementException;
import java.util.Optional;
import java.util.UUID;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;
import java.util.stream.Collectors;

/**
 * Generic in-memory repository.
 * Holds entities implementing {@link Identifiable}
 * The implementation is thread safe.
 */
public class InMem<T extends Identifiable> {
    private class LockedEntity {
        private T entity;
        private Lock lock = new ReentrantLock();

        public LockedEntity(T entity) {
            this.entity = entity;
        }

        public T getEntity() {
            return entity;
        }

        public Lock getLock() {
            return lock;
        }
    }
    /**
     * Local storage for storage
     */
    private ConcurrentHashMap<UUID, LockedEntity> storage = new ConcurrentHashMap<>();

    /**
     *
     * @return A full list of stored entities
     */
    public List<T> listAll() {
        return storage.values().stream()
                .peek(e -> {
                    e.getLock().lock();
                    e.getLock().unlock();
                })
                .map(LockedEntity::getEntity)
                .collect(Collectors.toList());
    }

    /**
     * Find an entity by specified key.
     * @throws NoSuchElementException In case of no entity with the key
     * @param id key of an entity to retrieve
     * @return entity
     */
    public T findByID(UUID id) {
        return Optional.ofNullable(storage.get(id))
                .map(e -> {
                    e.getLock().lock();
                    e.getLock().unlock();
                    return e.getEntity();
                }).orElseThrow();
    }

    /**
     * Store an entity into the local storage
     * Releases a lock if it's taken.
     * @param entity entity to store
     */
    public void save(T entity) {
        Optional.ofNullable(storage.get(entity.getId()))
                .ifPresentOrElse(e -> {
                    try {
                        e.getLock().unlock();
                    } catch (IllegalMonitorStateException error) {

                    }
                }, () -> storage.put(entity.getId(), new LockedEntity(entity)));
    }

    /**
     * Take a lock on stored entity.
     * A lock guarantees exclusive access.
     * @throws NoSuchElementException In case of no entity with the key
     * @param entity entity to take a lock on
     */
    public void lock(T entity) {
        Optional.ofNullable(storage.get(entity.getId()))
                .orElseThrow().getLock().lock();
    }

    /**
     * Release a lock on stored entity.
     * @throws NoSuchElementException In case of no entity with the key
     * @param entity entity to take a lock on
     */
    public void unlock(T entity) {
        try {
            Optional.ofNullable(storage.get(entity.getId()))
                    .orElseThrow().getLock().unlock();
        } catch (IllegalMonitorStateException e) {

        }
    }
}
