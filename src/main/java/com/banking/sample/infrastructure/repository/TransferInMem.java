package com.banking.sample.infrastructure.repository;

import com.banking.sample.domain.operations.Repository;
import com.banking.sample.domain.operations.Transfer;

/**
 * Im-memory {@link Transfer} repository
 */
public class TransferInMem extends InMem<Transfer> implements Repository {
}
