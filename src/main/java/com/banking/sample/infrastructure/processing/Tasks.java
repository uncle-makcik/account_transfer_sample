package com.banking.sample.infrastructure.processing;

/**
 * Asynchronous task scheduler interface
 */
public interface Tasks {
    /**
     * Schedule asynchronous process
     * @param process process to be executed
     */
    void schedule(Runnable process);

    /**
     * Graceful shutdown of the service
     */
    void shutdown();
}
