package com.banking.sample.infrastructure.processing;

import java.util.concurrent.Executors;
import java.util.concurrent.ThreadPoolExecutor;

/**
 * Asynchronous task executor. It performs no more then number of processors tasks simultaneously.
 */
public class TasksThreadPool implements Tasks {

    private ThreadPoolExecutor executor =
            (ThreadPoolExecutor) Executors.newFixedThreadPool(Runtime.getRuntime().availableProcessors());

    @Override
    public void schedule(Runnable process) {
        executor.submit(process);
    }

    @Override
    public void shutdown() {
        executor.shutdown();
    }

}
