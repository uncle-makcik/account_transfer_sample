package com.banking.sample.api.model;

import java.math.BigDecimal;
import java.util.Arrays;
import java.util.Objects;

import com.fasterxml.jackson.annotation.JsonProperty;

public class Money {

    private java.math.BigDecimal amount;
    private String currency;

    public Money () {
    }

    public Money(BigDecimal amount, String currency) {
        this.amount = amount;
        this.currency = currency;
    }

    public static Money of(com.banking.sample.domain.Money value) {
        return new Money(BigDecimal.valueOf(value.getAmount()), value.getCurrencyCode());
    }

    @JsonProperty("amount")
    public java.math.BigDecimal getAmount() {
        return amount;
    }

    public void setAmount(java.math.BigDecimal amount) {
        this.amount = amount;
    }

    @JsonProperty("currency")
    public String getCurrency() {
        return currency;
    }

    public void setCurrency(String currency) {
        this.currency = currency;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }

        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        Money Money = (Money) o;

        return Objects.equals(amount, Money.amount) &&
               Objects.equals(currency, Money.currency);
    }

    @Override
    public int hashCode() {
        return Objects.hash(amount, currency);
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append("class Money {\n");
        sb.append("    amount: ").append(toIndentedString(amount)).append("\n");
        sb.append("    currency: ").append(toIndentedString(currency)).append("\n");
        sb.append("}");
        return sb.toString();
    }

    /**
     * Convert the given object to string with each line indented by 4 spaces
     * (except the first line).
     */
    private String toIndentedString(Object o) {
        if (o == null) {
            return "null";
        }
        return o.toString().replace("\n", "\n    ");
    }

    /**
     * Convert to domain Money value
     * @return Equivalent domain Money value
     */
    public com.banking.sample.domain.Money asDomain() {
        return new com.banking.sample.domain.Money(getAmount().doubleValue(), getCurrency());
    }
}
