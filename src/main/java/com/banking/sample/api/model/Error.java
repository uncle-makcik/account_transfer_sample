package com.banking.sample.api.model;

import com.fasterxml.jackson.annotation.JsonProperty;

public class Error {
    private String message;

    public Error(String message) {
        this.message = message;
    }

    @JsonProperty("message")
    public String getMessage() {
        return message;
    }
}
