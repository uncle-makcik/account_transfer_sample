package com.banking.sample.api.model;

import java.time.LocalDateTime;
import java.util.Arrays;
import java.util.List;
import java.util.Objects;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;

@JsonInclude(JsonInclude.Include.NON_NULL)
public class Transfer {

    private String destination;
    private Money amount;
    private java.util.List<Link> links;
    private String id;
    private String source;
    private LocalDateTime created;

    public Transfer () {
    }

    public Transfer(String id, String source, String destination, Money amount, LocalDateTime created, List<Link> links) {
        this.destination = destination;
        this.amount = amount;
        this.links = links;
        this.id = id;
        this.source = source;
        this.created = created;
    }

    @JsonProperty("destination")
    public String getDestination() {
        return destination;
    }

    public void setDestination(String destination) {
        this.destination = destination;
    }

    @JsonProperty("amount")
    public Money getAmount() {
        return amount;
    }

    public void setAmount(Money amount) {
        this.amount = amount;
    }

    @JsonProperty("links")
    public java.util.List<Link> getLinks() {
        return links;
    }

    public void setLinks(java.util.List<Link> links) {
        this.links = links;
    }

    @JsonProperty("id")
    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    @JsonProperty("source")
    public String getSource() {
        return source;
    }

    public void setSource(String source) {
        this.source = source;
    }

    @JsonProperty("created")
    public LocalDateTime getCreated() {
        return created;
    }

    public void setCreated(LocalDateTime created) {
        this.created = created;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }

        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        Transfer Transfer = (Transfer) o;

        return Objects.equals(destination, Transfer.destination) &&
               Objects.equals(amount, Transfer.amount) &&
               Objects.equals(links, Transfer.links) &&
               Objects.equals(id, Transfer.id) &&
               Objects.equals(source, Transfer.source);
    }

    @Override
    public int hashCode() {
        return Objects.hash(destination, amount, links, id, source);
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append("class Transfer {\n");
        sb.append("    destination: ").append(toIndentedString(destination)).append("\n");
        sb.append("    amount: ").append(toIndentedString(amount)).append("\n");
        sb.append("    links: ").append(toIndentedString(links)).append("\n");
        sb.append("    id: ").append(toIndentedString(id)).append("\n");
        sb.append("    source: ").append(toIndentedString(source)).append("\n");
        sb.append("}");
        return sb.toString();
    }

    /**
     * Convert the given object to string with each line indented by 4 spaces
     * (except the first line).
     */
    private String toIndentedString(Object o) {
        if (o == null) {
            return "null";
        }
        return o.toString().replace("\n", "\n    ");
    }

    public static Transfer of(com.banking.sample.domain.operations.Transfer transfer) {
        return new Transfer(transfer.getId().toString(), transfer.getSource().getId().toString(),
                transfer.getDestination().getId().toString(), Money.of(transfer.getAmount()),
                transfer.getCreated(),
                List.of(Link.of(transfer.getSource(), "source"),
                        Link.of(transfer.getDestination(), "destination")));
    }
}
