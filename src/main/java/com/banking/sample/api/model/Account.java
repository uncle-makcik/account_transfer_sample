package com.banking.sample.api.model;

import java.time.LocalDateTime;
import java.util.Objects;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;

@JsonInclude(JsonInclude.Include.NON_NULL)
public class Account {

    private String name;
    private java.time.LocalDateTime registered;
    private String id;
    private Money balance;

    public Account () {
    }

    public Account(String name, LocalDateTime registered, String id, Money balance) {
        this.name = name;
        this.registered = registered;
        this.id = id;
        this.balance = balance;
    }

    public static Account of(com.banking.sample.domain.account.Account account) {
        return new Account(account.getName(), account.getRegistered(), account.getId().toString(),
                Money.of(account.getBalance().getValue()));
    }

    @JsonProperty("name")
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @JsonProperty("registered")
    public java.time.LocalDateTime getRegistered() {
        return registered;
    }

    public void setRegistered(java.time.LocalDateTime registered) {
        this.registered = registered;
    }

    @JsonProperty("id")
    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    @JsonProperty("balance")
    public Money getBalance() {
        return balance;
    }

    public void setBalance(Money balance) {
        this.balance = balance;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }

        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        Account Account = (Account) o;

        return Objects.equals(name, Account.name) &&
               Objects.equals(registered, Account.registered) &&
               Objects.equals(id, Account.id) &&
               Objects.equals(balance, Account.balance);
    }

    @Override
    public int hashCode() {
        return Objects.hash(name, registered, id, balance);
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append("class Account {\n");
        sb.append("    name: ").append(toIndentedString(name)).append("\n");
        sb.append("    registered: ").append(toIndentedString(registered)).append("\n");
        sb.append("    id: ").append(toIndentedString(id)).append("\n");
        sb.append("    balance: ").append(toIndentedString(balance)).append("\n");
        sb.append("}");
        return sb.toString();
    }

    /**
     * Convert the given object to string with each line indented by 4 spaces
     * (except the first line).
     */
    private String toIndentedString(Object o) {
        if (o == null) {
            return "null";
        }
        return o.toString().replace("\n", "\n    ");
    }
}
