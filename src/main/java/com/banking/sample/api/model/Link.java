package com.banking.sample.api.model;

import java.util.Arrays;
import java.util.Objects;

import com.banking.sample.domain.account.Account;
import com.fasterxml.jackson.annotation.JsonProperty;

public class Link {

    private String rel;
    private String type;
    private String href;

    public Link () {
    }

    public Link(String rel, String type, String href) {
        this.rel = rel;
        this.type = type;
        this.href = href;
    }

    @JsonProperty("rel")
    public String getRel() {
        return rel;
    }

    public void setRel(String rel) {
        this.rel = rel;
    }

    @JsonProperty("type")
    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    @JsonProperty("href")
    public String getHref() {
        return href;
    }

    public void setHref(String href) {
        this.href = href;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }

        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        Link Link = (Link) o;

        return Objects.equals(rel, Link.rel) &&
               Objects.equals(type, Link.type) &&
               Objects.equals(href, Link.href);
    }

    @Override
    public int hashCode() {
        return Objects.hash(rel, type, href);
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append("class Link {\n");
        sb.append("    rel: ").append(toIndentedString(rel)).append("\n");
        sb.append("    type: ").append(toIndentedString(type)).append("\n");
        sb.append("    href: ").append(toIndentedString(href)).append("\n");
        sb.append("}");
        return sb.toString();
    }

    /**
     * Convert the given object to string with each line indented by 4 spaces
     * (except the first line).
     */
    private String toIndentedString(Object o) {
        if (o == null) {
            return "null";
        }
        return o.toString().replace("\n", "\n    ");
    }

    public static Link of(Account account, String relation) {
        return new Link(relation, "GET", String.format("/v1/accounts/%s", account.getId()));
    }
}
