package com.banking.sample.api.handler;

import com.banking.sample.api.model.Account;
import com.banking.sample.api.model.Error;
import com.banking.sample.application.AccountService;
import com.banking.sample.domain.account.Repository;
import com.networknt.config.Config;
import com.networknt.handler.LightHttpHandler;
import com.networknt.service.SingletonServiceFactory;
import io.undertow.server.HttpServerExchange;
import io.undertow.util.HttpString;
import io.undertow.util.StatusCodes;

import java.util.HashMap;
import java.util.Map;
import java.util.NoSuchElementException;
import java.util.UUID;

public class AccountsAccountIdGetHandler implements LightHttpHandler {
    
    @Override
    public void handleRequest(HttpServerExchange exchange) throws Exception {
        String id = exchange.getQueryParameters().get("accountId").getFirst();

        Repository repository = SingletonServiceFactory.getBean(Repository.class);
        AccountService service = new AccountService(repository);
        var mapper = Config.getInstance().getMapper();
        String response = null;
        try {
            var account = service.get(UUID.fromString(id));
            exchange.setStatusCode(StatusCodes.OK);
            response = mapper.writeValueAsString(Account.of(account));
        } catch (NoSuchElementException e) {
            logger.error("Account not found", e);
            exchange.setStatusCode(StatusCodes.NOT_FOUND);
            response = mapper.writeValueAsString(new Error("Account not found"));
        }

        exchange.getResponseHeaders().add(new HttpString("Content-Type"), "application/json");
        exchange.getResponseSender().send(response);
        exchange.endExchange();
    }
}
