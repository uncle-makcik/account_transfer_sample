package com.banking.sample.api.handler;

import com.banking.sample.api.model.Account;
import com.banking.sample.application.AccountService;
import com.banking.sample.domain.account.Repository;
import com.networknt.config.Config;
import com.networknt.handler.LightHttpHandler;
import com.networknt.service.SingletonServiceFactory;
import io.undertow.server.HttpServerExchange;
import io.undertow.util.HttpString;
import io.undertow.util.StatusCodes;

import java.io.IOException;
import java.util.List;
import java.util.stream.Collectors;

public class AccountsGetHandler implements LightHttpHandler {
    
    @Override
    public void handleRequest(HttpServerExchange exchange) throws Exception {
        Repository repository = SingletonServiceFactory.getBean(Repository.class);
        AccountService service = new AccountService(repository);

        List<Account> response = service.list().stream().map(Account::of).collect(Collectors.toList());
        exchange.getResponseHeaders().add(new HttpString("Content-Type"), "application/json");
        exchange.setStatusCode(StatusCodes.OK);
        exchange.getResponseSender().send(Config.getInstance().getMapper().writeValueAsString(response));

        exchange.endExchange();
    }
}
