package com.banking.sample.api.handler;

import com.banking.sample.api.model.Account;
import com.banking.sample.api.model.Error;
import com.banking.sample.api.model.Transfer;
import com.banking.sample.application.TransferService;
import com.banking.sample.domain.operations.Repository;
import com.networknt.config.Config;
import com.networknt.handler.LightHttpHandler;
import com.networknt.service.SingletonServiceFactory;
import io.undertow.server.HttpServerExchange;
import io.undertow.util.HttpString;
import io.undertow.util.StatusCodes;

import java.util.HashMap;
import java.util.Map;
import java.util.NoSuchElementException;
import java.util.UUID;

public class TransfersTransferIdGetHandler implements LightHttpHandler {
    
    @Override
    public void handleRequest(HttpServerExchange exchange) throws Exception {
        String id = exchange.getQueryParameters().get("transferId").getFirst();

        var transferRepository = SingletonServiceFactory.getBean(Repository.class);
        var accountRepository = SingletonServiceFactory.getBean(com.banking.sample.domain.account.Repository.class);
        TransferService service = new TransferService(transferRepository, accountRepository);

        var mapper = Config.getInstance().getMapper();
        String response = null;

        try {
            var transfer = service.get(UUID.fromString(id));
            exchange.setStatusCode(StatusCodes.OK);
            response = mapper.writeValueAsString(Transfer.of(transfer));
        } catch (NoSuchElementException e) {
            logger.error("Transfer operation not found", e);
            exchange.setStatusCode(StatusCodes.NOT_FOUND);
            response = mapper.writeValueAsString(new Error("Transfer operation not found"));
        }

        exchange.getResponseHeaders().add(new HttpString("Content-Type"), "application/json");
        exchange.getResponseSender().send(response);
        exchange.endExchange();
    }
}
