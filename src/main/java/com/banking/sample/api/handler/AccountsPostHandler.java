package com.banking.sample.api.handler;

import com.banking.sample.api.model.Account;
import com.banking.sample.application.AccountService;
import com.banking.sample.domain.account.Repository;
import com.networknt.body.BodyHandler;
import com.networknt.config.Config;
import com.networknt.handler.LightHttpHandler;
import com.networknt.service.SingletonServiceFactory;
import io.undertow.server.HttpServerExchange;
import io.undertow.util.HttpString;
import io.undertow.util.StatusCodes;

import java.util.Map;

public class AccountsPostHandler implements LightHttpHandler {

    @SuppressWarnings("unchecked")
    @Override
    public void handleRequest(HttpServerExchange exchange) throws Exception {
        Repository repository = SingletonServiceFactory.getBean(Repository.class);
        AccountService service = new AccountService(repository);
        var mapper = Config.getInstance().getMapper();
        Map<String, Object> body = (Map<String, Object>) exchange.getAttachment(BodyHandler.REQUEST_BODY);
        Account request = mapper.convertValue(body, Account.class);
        var account = service.createAccount(request.getName(), request.getBalance().asDomain());
        Account response = Account.of(account);
        exchange.getResponseHeaders().add(new HttpString("Content-Type"), "application/json");
        exchange.setStatusCode(StatusCodes.CREATED);
        exchange.getResponseSender().send(mapper.writeValueAsString(response));
        exchange.endExchange();
    }
}
