package com.banking.sample.api.handler;

import com.banking.sample.api.model.Error;
import com.banking.sample.api.model.Transfer;
import com.banking.sample.application.TransferService;
import com.banking.sample.domain.operations.Repository;
import com.banking.sample.infrastructure.processing.Tasks;
import com.networknt.body.BodyHandler;
import com.networknt.config.Config;
import com.networknt.handler.LightHttpHandler;
import com.networknt.service.SingletonServiceFactory;
import io.undertow.server.HttpServerExchange;
import io.undertow.util.HttpString;
import io.undertow.util.StatusCodes;

import java.util.Map;
import java.util.NoSuchElementException;
import java.util.UUID;

public class TransfersPostHandler implements LightHttpHandler {

    @SuppressWarnings("unchecked")
    @Override
    public void handleRequest(HttpServerExchange exchange) throws Exception {
        var transferRepository = SingletonServiceFactory.getBean(Repository.class);
        var accountRepository = SingletonServiceFactory.getBean(com.banking.sample.domain.account.Repository.class);
        TransferService service = new TransferService(transferRepository, accountRepository);
        var mapper = Config.getInstance().getMapper();
        Map<String, Object> body = (Map<String, Object>) exchange.getAttachment(BodyHandler.REQUEST_BODY);
        Transfer request = mapper.convertValue(body, Transfer.class);

        String response = null;
        try {
            var transfer = service.create(UUID.fromString(request.getSource()), UUID.fromString(request.getDestination()),
                    request.getAmount().asDomain());
            var tasks = SingletonServiceFactory.getBean(Tasks.class);
            tasks.schedule(() -> {
                try {
                    service.perform(transfer.getId());
                } catch (Exception e) {
                    logger.error("Transfer failed", e);
                }
            });
            exchange.setStatusCode(StatusCodes.CREATED);
            response = mapper.writeValueAsString(Transfer.of(transfer));
        } catch (NoSuchElementException e) {
            logger.error("Account not found", e);
            exchange.setStatusCode(StatusCodes.NOT_FOUND);
            response = mapper.writeValueAsString(new Error("Account not found"));
        }
        exchange.getResponseHeaders().add(new HttpString("Content-Type"), "application/json");
        exchange.getResponseSender().send(response);
        exchange.endExchange();
    }
}
