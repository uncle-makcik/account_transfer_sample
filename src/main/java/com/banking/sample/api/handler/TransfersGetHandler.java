package com.banking.sample.api.handler;

import com.banking.sample.api.model.Account;
import com.banking.sample.api.model.Transfer;
import com.banking.sample.application.TransferService;
import com.banking.sample.domain.operations.Repository;
import com.networknt.config.Config;
import com.networknt.handler.LightHttpHandler;
import com.networknt.service.SingletonServiceFactory;
import io.undertow.server.HttpServerExchange;
import io.undertow.util.HttpString;
import io.undertow.util.StatusCodes;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

public class TransfersGetHandler implements LightHttpHandler {
    @Override
    public void handleRequest(HttpServerExchange exchange) throws Exception {
        var transferRepository = SingletonServiceFactory.getBean(Repository.class);
        var accountRepository = SingletonServiceFactory.getBean(com.banking.sample.domain.account.Repository.class);
        TransferService service = new TransferService(transferRepository, accountRepository);
        List<Transfer> response = service.list().stream().map(Transfer::of).collect(Collectors.toList());
        exchange.getResponseHeaders().add(new HttpString("Content-Type"), "application/json");
        exchange.setStatusCode(StatusCodes.OK);
        exchange.getResponseSender().send(Config.getInstance().getMapper().writeValueAsString(response));
        exchange.endExchange();
    }
}
