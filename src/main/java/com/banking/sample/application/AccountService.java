package com.banking.sample.application;

import com.banking.sample.domain.Money;
import com.banking.sample.domain.account.Account;
import com.banking.sample.domain.account.Balance;
import com.banking.sample.domain.account.Repository;

import java.util.List;
import java.util.Optional;
import java.util.UUID;

/**
 * Application service for Account entity.
 */
public class AccountService {
    Repository repository;

    public AccountService(Repository repository) {
        this.repository = repository;
    }

    /**
     * Create an account
     * @param name - name of a new account
     * @param initialBalance - initial balance, optional
     * @return new account
     */
    public Account createAccount(String name, Money initialBalance) {
        Account account = new Account.Builder()
                .setName(name)
                .setBalance(Optional.ofNullable(initialBalance)
                        .map(Balance::new)
                        .orElse(null))
                .setStatus(Account.Status.ENABLED)
                .build();
        repository.save(account);
        return account;
    }

    /**
     * List all accounts, registered in the system
     * @return List of accounts
     */
    public List<Account> list() {
        return repository.listAll();
    }

    /**
     * Find an account by given id
     * @param id - id of account to get
     * @return account that has been found
     */
    public Account get(UUID id) {
        return repository.findByID(id);
    }
}
