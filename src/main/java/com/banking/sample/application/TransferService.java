package com.banking.sample.application;

import com.banking.sample.domain.Money;
import com.banking.sample.domain.NotEnoughMoney;
import com.banking.sample.domain.account.Account;
import com.banking.sample.domain.account.IncompatibleState;
import com.banking.sample.domain.operations.OperationNotAllowed;
import com.banking.sample.domain.operations.Repository;
import com.banking.sample.domain.operations.Transfer;

import java.util.List;
import java.util.NoSuchElementException;
import java.util.UUID;

/**
 * Application service for Transfer operation entity.
 */
public class TransferService {
    private Repository operationRepository;
    private com.banking.sample.domain.account.Repository accountRepository;

    public TransferService(Repository operationRepository, com.banking.sample.domain.account.Repository accountRepository) {
        this.operationRepository = operationRepository;
        this.accountRepository = accountRepository;
    }

    /**
     * List all accounts, registered in the system
     * @return List of accounts
     */
    public List<Transfer> list() {
        return operationRepository.listAll();
    }

    /**
     * Find a transfer operation by given id
     * @param id - id of transfer operation to get
     * @return transfer that has been found
     */
    public Transfer get(UUID id) {
        return operationRepository.findByID(id);
    }

    /**
     * Create a transfer operation.
     * Created operation is not performed. It's queued to be performed by an operation processor.
     * @param sourceID - source account ID
     * @param destinationID - destination account ID
     * @param amount - amount of money to be transfered
     * @return Created Transfer
     */
    public Transfer create(UUID sourceID, UUID destinationID, Money amount) {
        final Account source = accountRepository.findByID(sourceID);
        final Account destination = accountRepository.findByID(destinationID);
        Transfer transfer = new Transfer(source, destination, amount);
        operationRepository.save(transfer);
        return transfer;
    }

    /**
     * Performs transfer operation
     * @param id id of operation to be performed
     * @throws NoSuchElementException transfer operation is not created
     * @throws OperationNotAllowed attempt to perform transfer more then once
     * @throws NotEnoughMoney - source account balance is not enough
     * @throws IncompatibleState - either source or destination is not enabled
     * @return Updated transfer operation
     */
    public Transfer perform(UUID id) throws OperationNotAllowed, NotEnoughMoney, IncompatibleState {
        Transfer transfer = operationRepository.findByID(id);
        try {
            operationRepository.lock(transfer);
            accountRepository.lock(transfer.getSource());
            accountRepository.lock(transfer.getDestination());
            transfer.perform();
            operationRepository.save(transfer);
        } finally {
            accountRepository.unlock(transfer.getSource());
            accountRepository.unlock(transfer.getDestination());
            operationRepository.unlock(transfer);
        }
        return transfer;
    }

    /**
     * Cancel performed transfer operation
     * @throws OperationNotAllowed attempt to revert not performed transfer
     * @throws NotEnoughMoney - destination account balance is not enough
     * @throws IncompatibleState - either source or destination is not enabled
     * @param id - id of transfer operation to be cancelled
     */
    public void revert(UUID id) throws NotEnoughMoney, OperationNotAllowed, IncompatibleState {
        Transfer transfer = operationRepository.findByID(id);
        transfer.revert();
        operationRepository.save(transfer);
    }
}
